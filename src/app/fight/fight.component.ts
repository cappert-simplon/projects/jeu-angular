import {Component, Input, OnInit} from '@angular/core';
import {Gladiator} from "../entities/gladiator";

@Component({
  selector: 'app-fight',
  templateUrl: './fight.component.html',
  styleUrls: ['./fight.component.css']
})
export class FightComponent implements OnInit {
  // ?? comment créer un Gladiator?
  @Input()
  opponents?:Gladiator[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  resolve() {
  // algo qui fait combattre les combattants (dans opponents)
  }
}
