export class Gladiator {
  name :string;
  hp:number;
  stamina:number;

  constructor(name :string,  hp:number,  stamina:number) {
    this.name = name;
    this.hp = hp;
    this.stamina = stamina;
  }
}
