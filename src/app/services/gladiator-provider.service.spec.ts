import { TestBed } from '@angular/core/testing';

import { GladiatorProviderService } from './gladiator-provider.service';

describe('GladiatorProviderService', () => {
  let service: GladiatorProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GladiatorProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
