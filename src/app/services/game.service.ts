import { Injectable } from '@angular/core';
import {Gladiator} from "../entities/gladiator";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  currentPlayer?:Gladiator;

  // pass ChapterComponents around? or store Chapter as an Entity
  currentChapter:Chapter;

  constructor() { }
}
