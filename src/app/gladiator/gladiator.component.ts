import {Component, Input, OnInit} from '@angular/core';
import {Gladiator} from "../entities/gladiator";

@Component({
  selector: 'app-gladiator',
  templateUrl: './gladiator.component.html',
  styleUrls: ['./gladiator.component.css']
})
export class GladiatorComponent implements OnInit {

  @Input()
  opponent?:Gladiator;

  //TODO GladiatorService?
  constructor() {
  }

  ngOnInit(): void {
  }

}
