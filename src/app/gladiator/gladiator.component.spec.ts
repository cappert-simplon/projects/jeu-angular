import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GladiatorComponent } from './gladiator.component';

describe('GladiatorComponent', () => {
  let component: GladiatorComponent;
  let fixture: ComponentFixture<GladiatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GladiatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GladiatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
