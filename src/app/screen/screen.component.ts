import {Component, Input, OnInit} from '@angular/core';
import {Gladiator} from "../entities/gladiator";
import {ScreenService} from "../services/screen.service";

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.css']
})
export class ScreenComponent implements OnInit {

  //TODO @Input?
  @Input()
  opponents: Gladiator[] = [];

  fightInProgress:boolean = false;

  @Input()
  imageUrl?:string;

  @Input()
  text:string = '';

  @Input()
  actions?:string[];

  constructor(screenService :ScreenService) { }

  ngOnInit(): void {
  }

}
