Simulation de gladiateurs, les combats sont résolus automatiquement et le joueur va principalement s'occuper de la stratégie d'entrainement, etc de son gladiateur.
https://gitlab.com/cappert-simplon/projects/gladiator

Barthélémy DELUY - 06/04/22

Salut !
Quelques remarques :
* le jeu crashe après le 2ème combat, à cause du parseInt du Scanner (documenté dans ton code) => le souci vient du parseInt, il faut utiliser nextLine(), et caster le résultat vers un int
* il y a des imports inutilisés, un constructeur et des ressources dans le App.java inutiles aussi : attention au code mort ! Grâce aux commits, on peut facilement retrouver de l'ancien code, il ne faut pas hésiter à faire le ménage :-)
* des soucis d'indentation et de choix de langue pour les méthodes (attention à la norme que vous avez choisie :) )
* les méthodes sont parfois trop longues; dans l'idéal, on essaie de les faire tenir sur un seul écran pour limiter les bugs (par exemple, avoir des méthodes pour l'affichage des messages, ça permet de changer les messages sans toucher à la logique du programme)
* le découpage des classes et des méthodes est top, les noms sont parlants, la visibilité est bien choisie

En résumé : du bon travail, quelques petits détails à améliorer au cours de la formation au niveau de l'organisation du code, mais tu as déjà de très bons acquis.

compétence validée: C7. Développer la partie back-end - niveau 1

