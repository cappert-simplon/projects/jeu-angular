# JeuAngular
Simulation de carrière de gladiateur en mode textuel avec des éléments RPG.
Non-fonctionnel mais l'ossature de l'archi est en place.
Conversion du [projet Java](https://gitlab.com/cappert-simplon/projects/gladiator) en Angular/Typescript

<figure>
<figcaption><h2>Ébauche de diagramme.</h2></figcaption>
<img src="schema-jeu-angular.drawio.png">
</figure>

## Architecture du code

### /app.component.ts
point d'entrée qui va seulement contenir les instances de `ScreenComponent`.
### /entities/gladiator.ts
Entité qui réprésente un gladiateur, soit le `currentPlayer` soit un adversaire.

## Services
### /services/game.service.ts
Stocke l'état du jeu, on pourrait le mettre dans un savefile sur le disque ou dans le LocalStorage en JS

### /services/gladiator-provider.service.ts
gère les échanges de données entre le composant GladiatorComponent avec une hypothétique API sur un serveur. 

### /services/screen.service.ts
Provider pour le ScreenComponent

## Components

ces différents composants sont imbriqués les uns dans les autres.

### /screen/screen.component.ts
représente ce qu'affiche un "écran" comme dans un rpg à l'ancienne, avec une `image`, du `text` et des boutons (`actions`)

### /chapter/chapter.component.ts
chapitre de l'histoire qui va stocker les données par rapport à l'histoire

### /fight/fight.component.ts
classe qui organise les combats entre les `Gladiator`s

### /gladiator/gladiator.component.ts
affiche les infos relatives aux gladiateurs dans le `Fight`
